<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ URL::asset('assets/img/default.jpg') }}" class="img-circle" alt="User Image">

            </div>
            {{-- <div class="pull-left info">
                <p>Kang Dru</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div> --}}
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="@if (Route::currentRouteName()=='Home') active @endif">
                <a href="{{route('Home')}}"><i class="fa fa-home"></i> <span>Home</span></a>
            </li>

            @php($dat = array(1))
            @if (in_array(Auth::user()->role_id,$dat)) 
                <li class="treeview @if (Route::currentRouteName()=='ListAdministrator' OR Route::currentRouteName()=='AddAdministrator') active menu-open @endif">
                    <a href="#">
                        <i class="fa fa-laptop"></i> <span>Administrator</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="@if (Route::currentRouteName()=='AddAdministrator') active @endif"><a href="{{route('AddAdministrator')}}"><i class="fa fa-circle-o"></i> Add Administrator</a></li>
                        <li class="@if (Route::currentRouteName()=='ListAdministrator') active @endif"><a href="{{route('ListAdministrator')}}"><i class="fa fa-circle-o"></i> List Administrator</a></li>
                    </ul>
                </li>        
            @endif


            
            <li class="treeview @if (Route::currentRouteName()=='ListNasabah' OR Route::currentRouteName()=='AddNasabah') active menu-open @endif">
                <a href="#">
                    <i class="fa fa-table"></i> <span>Nasabah</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="@if (Route::currentRouteName()=='AddNasabah') active @endif"><a href="{{route('AddNasabah')}}"><i class="fa fa-circle-o"></i> Add Nasabah</a></li>
                    <li class="@if (Route::currentRouteName()=='ListNasabah') active @endif"><a href="{{route('ListNasabah')}}"><i class="fa fa-circle-o"></i> List Nasabah</a></li>
                </ul>
            </li>

            @php($dat = array(1,2))
            @if (in_array(Auth::user()->role_id,$dat)) 
                <li class="treeview @if (Route::currentRouteName()=='ListTransaksi' OR Route::currentRouteName()=='AddTransaksi') active menu-open @endif">
                    <a href="#">
                        <i class="fa fa-bar-chart-o"></i> <span>Transaksi</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="@if (Route::currentRouteName()=='AddTransaksi') active @endif"><a href="AddTransaksi"><i class="fa fa-circle-o"></i> Add Transaksi</a></li>
                        <li class="@if (Route::currentRouteName()=='ListTransaksi') active @endif"><a href="ListTransaksi"><i class="fa fa-circle-o"></i> List Transaksi</a></li>
                        <li><a href=""><i class="fa fa-circle-o"></i> Costum Transaksi</a></li>
                    </ul>
                </li>
                <li class="treeview @if (Route::currentRouteName()=='ListPatner') active menu-open @endif">
                    <a href="#">
                        <i class="fa fa-bar-chart-o"></i> <span>Partners</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="@if (Route::currentRouteName()=='AddPartner') active @endif"><a href="{{route('AddPartner')}}"><i class="fa fa-circle-o"></i> Add Partner</a></li>
                        <li class="@if (Route::currentRouteName()=='ListPartner') active @endif"><a href="{{route('ListPartner')}}"><i class="fa fa-circle-o"></i> List Partner</a></li>
                        {{-- <li class="@if (Route::currentRouteName()=='TrfPartner') active @endif"><a href="{{route('TrfPartner')}}"><i class="fa fa-circle-o"></i> Transfer Partner</a></li> --}}
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-table"></i> <span>Course</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href=""><i class="fa fa-circle-o"></i> Add Course</a></li>
                        <li><a href=""><i class="fa fa-circle-o"></i> List Course</a></li>
                        <li><a href=""><i class="fa fa-circle-o"></i> List Peserta</a></li>
                        <li><a href=""><i class="fa fa-circle-o"></i> Courser Payment</a></li>
                    </ul>
                </li>
                <li class="treeview @if (Route::currentRouteName()=='ReportTransaksi' OR Route::currentRouteName()=='CustomReport') active menu-open @endif">
                    <a href="#">
                        <i class="fa fa-table"></i> <span>Report</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="@if (Route::currentRouteName()=='ReportTransaksi') active @endif"><a href="{{route('ReportTransaksi')}}"><i class="fa fa-circle-o"></i> Report Transaksi</a></li>
                        <li class="@if (Route::currentRouteName()=='CustomReport') active @endif"><a href="{{route('CustomReport')}}"><i class="fa fa-circle-o"></i> Custom Reports</a></li>
                    </ul>
                </li>
                <li class="treeview @if (Route::currentRouteName()=='CommisionReport' OR Route::currentRouteName()=='ListCommission') active menu-open @endif">
                    <a href="#">
                        <i class="fa fa-table"></i> <span>Commission</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="@if (Route::currentRouteName()=='CommisionReport') active @endif"><a href="{{route('CommisionReport')}}"><i class="fa fa-circle-o"></i> Add Commision </a></li>
                        <li class="@if (Route::currentRouteName()=='ListCommission') active @endif"><a href="{{route('ListCommission')}}"><i class="fa fa-circle-o"></i> List Commission Reports</a></li>

                        <li class="@if (Route::currentRouteName()=='TransactionReport') active @endif"><a href="{{route('TransactionReport')}}"><i class="fa fa-circle-o"></i> Transaction report</a></li>

                        
                    </ul>
                </li>

                <li class="treeview @if (Route::currentRouteName()=='UploadExcel') active menu-open @endif">
                    <a href="#">
                        <i class="fa fa-table"></i> <span>Upload Excel</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="@if (Route::currentRouteName()=='UploadExcel') active @endif"><a href="{{route('UploadExcel')}}"><i class="fa fa-circle-o"></i> Upload Excel </a></li>
                    </ul>
                </li>

            @endif            
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
