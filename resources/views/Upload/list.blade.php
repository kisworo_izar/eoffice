@extends('main')
@section('content')

<section class="content-header">
    <h1>Upload Data Excel</small></h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-laptop"></i> Upload Excel</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

        @csrf
        @if ($message = Session::get('success'))
        <div id="successMessage" class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>    
            <strong>{{ $message }}</strong>
        </div>
        @endif
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Upload Excel</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form action="{{ route('file-import') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group mb-4" style="max-width: 500px; margin: 0 auto;">
                        <div class="custom-file text-left">
                            <input type="file" name="uploaded_file" class="custom-file-input" id="uploaded_file">
                        </div>
                    </div>
                    <button  type="submit" class="btn btn-primary">Import data</button>
                </form>
            </div>

            <div class="box-footer">
                {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
            </div>
            <!-- /.box-body -->
        </div>

</section>
    
@stop
