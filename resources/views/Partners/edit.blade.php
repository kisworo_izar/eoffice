@extends('main')
@section('content')

<section class="content-header">
    <h1>Partners <small>Edit Partner</small></h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-laptop"></i> Edit Partner</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

<form action="{{route('saveeditpartner')}}" method="POST" enctype="multipart/form-data">
    @csrf
    @if ($message = Session::get('success'))
    <div id="successMessage" class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
    @endif
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Edit Partner</h3>
        </div>
        <input type="hidden" name="ueid" value="{{$arr->id}}">
        <!-- /.box-header -->
        <div class="box-body">

            <div class="form-group">
                <label for="AdministratorName">Nama Partner</label>
                <input type="text" class="form-control" name="partner_nama" placeholder="Nama Partner" value="{{$arr->partner_nama}}">
            </div>

            <div class="form-group">
                <label for="AdministratorName">Nama Group </label>
                <input type="text" class="form-control" name="partner_group" placeholder="Nama Group" value="{{$arr->partner_group}}">
            </div>

            <div class="form-group">
                <label for="EmailAdress">Email</label>
                <input type="text" class="form-control" name="partner_email" placeholder="Email" value="{{$arr->partner_email}}">
            </div>

            <div class="form-group">
                <label for="Password">No. HP</label>
                <input type="text" class="form-control" name="partner_hp" placeholder="No. HP" value="{{$arr->partner_hp}}">
            </div>

        </div>

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </div>
</form>

</section>

<script>
    setTimeout(function() {
        $('#successMessage').fadeOut('fast');
    }, 5000);

</script>


@stop