@extends('main')
@section('content')

<section class="content-header">
    <h1>Partners <small>Add Partner</small></h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-laptop"></i> Add Partner</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

<form action="{{route('addpartner')}}" method="POST" enctype="multipart/form-data">
    @csrf
    @if ($message = Session::get('success'))
    <div id="successMessage" class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
    @endif
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Add Partner</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <div class="form-group">
                <label for="AdministratorName">Nama Partner</label>
                <input type="text" class="form-control" name="partner_nama" placeholder="Nama Partner">
            </div>

            <div class="form-group">
                <label for="AdministratorName">Nama Group </label>
                <input type="text" class="form-control" name="partner_group" placeholder="Nama Group">
            </div>

            <div class="form-group">
                <label for="EmailAdress">Email</label>
                <input type="text" class="form-control" name="partner_email" placeholder="Email">
            </div>

            <div class="form-group">
                <label for="Password">No. HP</label>
                <input type="text" class="form-control" name="partner_hp" placeholder="No. HP">
            </div>

        </div>

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>

</section>

<script>
    setTimeout(function() {
        $('#successMessage').fadeOut('fast');
    }, 5000);

</script>


@stop