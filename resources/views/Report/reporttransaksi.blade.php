@extends('main')
@section('content')

<section class="content-header">
    <h1>Report Transaksi <small>Control Panel</small></h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-laptop"></i> Report Transaksi</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">

        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Total Transaksi Bulan Desember</h3>
                </div>

                <div class="box-body chart-responsive">
                    <div class="chart" id="sales-chart" style="height: 300px; position: relative;"></div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Top Value Transaksi Bulan Desember</h3>
                </div>

                <div class="box-body">
                    <table id="table-data" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID Nasabah</th>
                                <th>Nama Nasabah</th>
                                <th>Total Buy</th>
                                <th>Total Sell</th>
                                <th>Total Transaksi</th>
                                <th>Komisi</th>
                                <th>Group</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php($no = 0)
                            @forelse ($arr as $key => $row)
                            @php($no++)
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{$row['id_nasabah']}}</td>
                                    <td>{{$row['nama_nasabah']}}</td>
                                    <td>{{$row['transaksi_buy']}}</td>
                                    <td>{{$row['transaksi_sell']}}</td>
                                    <td>{{$row['total']}}</td>
                                    <td>{{$row['transaksi_komisi']}}</td>
                                    <td>{{$row['status_nasabah']}}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No Data Available</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
      $('#table-data').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : true
      })
    })
</script>

<script src="{{ URL::asset('assets/raphael/raphael.min.js') }}"></script>
<script src="{{ URL::asset('assets/morris.js/morris.min.js') }}"></script>

<script>

$(function () {
    "use strict";
        var total  = {!! json_encode($total) !!};
        console.log(total)
        var donut = new Morris.Donut({
            element: 'sales-chart',
            resize: true,
            colors: ["#3c8dbc", "#f6725f", "#00a65a", "#333333", "#333333", "#333333", "#333333"],
            data: total,
            hideHover: 'auto'
        });
    });
</script>


@stop