@extends('main')
@section('content')

<section class="content-header">
    <h1>Commission <small>Edit Commission</small></h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-laptop"></i> Edit Commission</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

<form action="{{route('saveeditcommission')}}" method="POST" enctype="multipart/form-data">
    @csrf
    @if ($message = Session::get('success'))
    <div id="successMessage" class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
    @endif
    <div class="box box-primary">
        <input type="hidden" name="_id" value="{{$arr->id}}">
        <div class="box-header">
            <h3 class="box-title">Edit Commission</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <label>Bulan</label>
            <div id="datepicker" class="input-group date" data-date-format="yyyy-mm-dd">
                <input class="form-control" name="bulan_komisi" type="text" value="{{$arr->bulan_komisi}}" />
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>

            <div class="form-group">
                <label>Jumlah Komisi</label>
                <input type="text" class="form-control" name="jumlah_komisi" placeholder="Jumlah Komisi" value="{{$arr->jumlah_komisi}}">
            </div>

            <div class="form-group">
                <label>Bukti Transfer</label> <br>
                <label><a href="{{asset('data_file/'.$arr->bukti_komisi)}}" target="_blank"> {{$arr->bukti_komisi}}</a> </label>
                <input type="file" class="form-control" name="file" value="{{$arr->bukti_komisi}}" >
            </div>


        </div>

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>

</section>

<script>
    setTimeout(function() {
        $('#successMessage').fadeOut('fast');
    }, 5000);

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<script>
       var date = $('#datepicker').datepicker({ dateFormat: 'yy-dd-mm' }).val();
</script>


@stop