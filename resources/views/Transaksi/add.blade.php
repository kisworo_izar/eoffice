@extends('main')
@section('content')

<section class="content-header">
    <h1>Transaksi <small>Add Transaksi</small></h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-laptop"></i> Add Transaksi</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <form action="{{route('saveaddtransaksi')}}" method="POST" enctype="multipart/form-data">
        @csrf
        @if ($message = Session::get('success'))
        <div id="successMessage" class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>    
            <strong>{{ $message }}</strong>
        </div>
        @endif
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Add Transaksi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="form-group">
                    <label for="IDNasabah">ID Nasabah</label>
                    <input type="text" class="form-control" id="IDNasabah" name="id_nasabah" placeholder="ID Nasabah">
                </div>

                <div class="form-group">
                    <label>Tanggal Transaksi:</label>
                    <div id="datepicker" class="input-group date" data-date-format="yyyy-mm-dd">
                        <input class="form-control" type="text" id="transaksi_date" name="transaksi_date" />
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="TransaksiBuy">Transaksi Buy</label>
                    <input type="text" class="form-control" name="transaksi_buy" id="TransaksiBuy" placeholder="Transaksi Buy">
                </div>

                <div class="form-group">
                    <label for="TransaksiSell">Transaksi Sell</label>
                    <input type="text" class="form-control" name="transaksi_sell" id="TransaksiSell" placeholder="Transaksi Sell">
                </div>

                <div class="form-group">
                    <label for="KomisiTransaksi">Komisi Transaksi </label>
                    <input type="text" class="form-control" name="transaksi_komisi" id="KomisiTransaksi" placeholder="TKomisiransaksi ">
                </div>


            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <!-- /.box-body -->
        </div>
    </form>

</section>

<script>
var date = $('#transaksi_date').datepicker({ dateFormat: 'yy-dd-mm' }).val();
</script>

{{-- <script src="{{ URL::asset('assets/datepicker/bootstrap-datepicker.js')}}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<script>
    $(function () {
        $("#datepicker").datepicker({ 
                autoclose: true, 
                todayHighlight: true
        }).datepicker('update', new Date());
    });
</script>


@stop
