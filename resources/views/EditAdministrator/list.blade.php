@extends('main')
@section('content')

<section class="content-header">
    <h1>Administrator <small>Edit Administrator</small></h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-laptop"></i> Edit Administrator</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

<form action="{{route('saveeditadmin')}}" method="POST" enctype="multipart/form-data">
    @csrf
    @if ($message = Session::get('success'))
    <div id="successMessage" class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
    @endif
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Edit Administrator</h3>
        </div>
        <input type="hidden" name="user_id" value="{{$arr->id}}">
        <!-- /.box-header -->
        <div class="box-body">

            <div class="form-group">
                <label for="AdministratorName">Administrator ID</label>
                <input type="text" class="form-control" name="admin_id" placeholder="Administrator Name" value="{{$arr->_id}}" required>
            </div>

            <div class="form-group">
                <label for="AdministratorName">Administrator Name</label>
                <input type="text" class="form-control" name="admin_name" placeholder="Administrator Name" value="{{$arr->name}}" required>
            </div>

            <div class="form-group">
                <label for="EmailAdress">EmailAddress</label>
                <input type="text" class="form-control" name="admin_email" placeholder="EmailAddress" value="{{$arr->email}}" required>
            </div>

            <div class="form-group">
                <label for="Password">Password</label>
                <input type="password" class="form-control" name="admin_pass" placeholder="Password" value="" required>
            </div>

        </div>

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>

</section>

<script>

</script>


@stop