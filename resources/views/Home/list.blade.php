@extends('main')
@section('content')

<section class="content-header">
    <h1>Home <small>Control panel</small></h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-home"></i> Home</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">

        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?php if($nasabah=='') echo '-';else echo $nasabah; ?></h3>
                    <p>Nasabah</p>
                </div>
              <div class="icon">
                    <i class="ion ion-person-add"></i>
              </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?php if($komisi_skg=='') echo '-';else echo $komisi_skg; ?></h3>
                    <p>Net Komisi Bulan {{$bulanskg}}</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>



        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?php if($total_transaksi=='') echo '-';else echo $total_transaksi; ?></h3>
                    <p>Transaksi Bulan {{$bulanskg}}</p>
                </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?php if($komisi_lalu=='') echo '-';else echo $komisi_lalu; ?></h3>
                    <p>Net komisi Bulan {{$bulanlalu}}</p>
                </div>
            <div class="icon">
                <i class="ion ion-pie-graph"></i>
            </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

    </div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">List Nasabah</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="table-data" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID Nasabah</th>
                        <th>Nama Nasabah</th>
                        <th>No HP</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($nsb as $row)
                        <tr>
                            <td>{{$row->id_nasabah}}</td>
                            <td>{{$row->nama_nasabah}}</td>
                            <td>{{$row->hp_nasabah}}</td>
                            <td>{{$row->status_nasabah}}</td>
                        </tr>
                    @empty
                        <tr>
                            <td>No Data Available</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
</section>

<script>
    $(function () {
      $('#table-data').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : true
      })
    })
</script>


@stop