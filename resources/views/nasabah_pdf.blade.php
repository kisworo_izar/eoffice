<html>
<head>
	<title>Membuat Laporan PDF Nasabah</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan List Nasabah</h4>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>ID Nasabah</th>
				<th>Nama Nasabah</th>
				<th>No HP</th>
				<th>Keterangan</th>
				<th>Jenis Nasabah</th>
				<th>Kode Sales</th>
			</tr>
		</thead>
		<tbody>
            @php($no = 0)
            @foreach ($nasabah as $key => $row)
                @php($no++)
                <tr>
                    <td>{{$no}}</td>
                    <td>{{$row->id_nasabah}}</td>
                    <td>{{$row->nama_nasabah}}</td>
                    <td>{{$row->hp_nasabah}}</td>
                    <td>{{$row->status_nasabah}}</td>
                    <td>{{ucwords($row->jenis_nasabah)}}</td>
                    <td>{{$row->kodesales_nasabah}}</td>
                </tr>
                @endforeach      
		</tbody>
	</table>
 
</body>
</html>