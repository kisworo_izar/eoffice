@extends('main')
@section('content')

<section class="content-header">
    <h1>Nasabah <small>Add Nasabah</small></h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-laptop"></i> Add Nasabah</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <form action="{{route('saveaddnasabah')}}" method="POST" enctype="multipart/form-data">
        @csrf
        @if ($message = Session::get('success'))
        <div id="successMessage" class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>    
            <strong>{{ $message }}</strong>
        </div>
        @endif
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Add Nasabah</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="form-group">
                    <label for="IDNasabah">ID Nasabah</label>
                    <input type="text" class="form-control" id="IDNasabah" name="id_nasabah" placeholder="ID Nasabah">
                </div>

                <div class="form-group">
                    <label for="NasabahName">Nasabah Name</label>
                    <input type="text" class="form-control" id="NasabahName" name="nama_nasabah" placeholder="Nasabah Name">
                </div>

                <div class="form-group">
                    <label for="nohp">No HP</label>
                    <input type="text" class="form-control" id="nohp" name="hp_nasabah" placeholder="No HP">
                </div>

                <div class="form-group">
                    <label for="nohp">Jenis Nasabah</label>
                    <select id="jenis_nasabah" name="jenis_nasabah" class="form-control">
                        @foreach ($jns as $key => $row)
                            <option value="{{$row}}">{{ucwords($row)}} </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group" id="kodesales_nasabah" style="display: none">
                    <label for="nohp">Kode Sales</label>
                    <select name="kodesales_nasabah" class="form-control" >
                        @foreach ($sls as $key => $row)
                            <option value="{{$row}}">{{$row}} </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Group</label>
                    <select name="status_nasabah" class="form-control">
                        <option value="CTH">CTH </option>
                        <option value="Kefas">Kefas </option>
                        <option value="General">General </option>
                        <option value="Prasmul Alumni">Prasmul Alumni </option>
                        <option value="CTH-Client">CTH-Client </option>
                        <option value="Kata Dhana">Kata Dhana </option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Timestamp:</label>
                    <div id="datepicker" class="input-group date" data-date-format="yyyy-mm-dd">
                        <input class="form-control" type="text" id="timestamp_nasabah" name="timestamp_nasabah" />
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>


            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <!-- /.box-body -->
        </div>
    </form>
</section>
<script>
    $(function () {
        $("#jenis_nasabah").change(function() {
            var val = $(this).val();
            if(val === "pindah") {
                $("#kodesales_nasabah").show();
            }
            else {
                $("#kodesales_nasabah").hide();
            }
        });
    });
</script>

<script>
    setTimeout(function() {
        $('#successMessage').fadeOut('fast');
    }, 2000);

</script>

<script>
    var date = $('#timestamp_nasabah').datepicker({ dateFormat: 'yy-dd-mm' }).val();
    </script>
    
    {{-- <script src="{{ URL::asset('assets/datepicker/bootstrap-datepicker.js')}}"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    
    <script>
        $(function () {
            $("#datepicker").datepicker({ 
                    autoclose: true, 
                    todayHighlight: true
            }).datepicker('update', new Date());
        });
    </script>
    
@stop
