@extends('main')
@section('content')

<section class="content-header">
    <h1>Patners <small>List Patners</small></h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-laptop"></i> List Patner</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">List Nasabah</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="table-data" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Patner</th>
                        <th>Nama Group</th>
                        <th>No HP</th>
                        <th>Email</th>
                        <th>Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @for ($i=1;$i<=100;$i++)
                        
                        <tr>
                            <td>{{$i}}</td>
                            <td>Kang Dru{{$i}}</td>
                            <td>CTH</td>
                            <td>08121111{{$i}}</td>
                            <td>kang{{$i}}@gmail.com</td>
                            <td>
                                <button type="button" class="btn btn-primary">Edit</button>
                                <button type="button" class="btn btn-danger">Delete</button>
                            </td>
                        </tr>

                    @endfor
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
</section>

<script>
    $(function () {
      $('#table-data').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : true
      })
    })
</script>


@stop