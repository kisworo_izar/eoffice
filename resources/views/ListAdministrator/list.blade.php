@extends('main')
@section('content')

<section class="content-header">
    <h1>Administrator <small>List Administrator</small></h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-laptop"></i> List Administrator</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
@csrf
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">List Administrator</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="table-data" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Administrator ID</th>
                        <th>Administrator Name</th>
                        <th>Administrator Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php($no = 0)
                    @forelse ($arr as $key => $row)
                    @php($no++)
                        <tr>
                            <td>{{$no}}</td>
                            <td>{{$row->_id}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->email}}</td>
                            <td>
                                <a href="{{route('editadmin',$row->id)}}" type="button" class="btn btn-primary">Edit</a>
                                <button type="button" data-item="{{$row->id}}" class="btn btn-danger delete">Delete</button>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td>No Data Available</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
</section>

<script>
    $(function () {
      $('#table-data').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : true
      })
    })
</script>

<script>
    $(document).on('click', '.delete', function () {
        Swal.fire({
            title: '',
            text: "Are  you sure want to delete this item ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    var id = $(this).attr('data-item');
                    var tbl= 'users';

                    $.ajax({
                        type:'POST',
                        url:"{{ route('delete_table') }}",
                        data:{
                            _token: "{{ csrf_token() }}",
                            id:id,
                            tbl:tbl
                        },
                        success:function(data){
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Delete Success !',
                                showConfirmButton: false,
                                timer: 1500
                            }).then(function () {
                                location.reload();
                            });
                        }
                    });
                }
            })
    });

</script>


@stop