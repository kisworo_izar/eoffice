@extends('main')
@section('content')

<section class="content-header">
    <h1>Nasabah <small>Edit Nasabah</small></h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-laptop"></i> Edit Nasabah</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <form action="{{route('saveeditnasabah')}}" method="POST" enctype="multipart/form-data">
        @csrf
        @if ($message = Session::get('success'))
        <div id="successMessage" class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>    
            <strong>{{ $message }}</strong>
        </div>
        @endif
        <div class="box box-primary">
            <input type="hidden" name="ueid" value="{{$arr->id}}">
            <div class="box-header">
                <h3 class="box-title">Edit Nasabah</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="form-group">
                    <label for="IDNasabah">ID Nasabah</label>
                    <input type="text" class="form-control" id="IDNasabah" name="id_nasabah" placeholder="ID Nasabah" value="{{$arr->id_nasabah}}">
                </div>

                <div class="form-group">
                    <label for="NasabahName">Nasabah Name</label>
                    <input type="text" class="form-control" id="NasabahName" name="nama_nasabah" placeholder="Nasabah Name" value="{{$arr->nama_nasabah}}">
                </div>

                <div class="form-group">
                    <label for="nohp">No HP</label>
                    <input type="text" class="form-control" id="nohp" name="hp_nasabah" placeholder="No HP" value="{{$arr->hp_nasabah}}">
                </div>

                <div class="form-group">
                    <label>Jenis Nasabah</label>
                    <input type="text" class="form-control" value="{{ucwords($arr->jenis_nasabah)}}" readonly>
                </div>

                <div class="form-group">
                    <label for="jenis_nasabah">Ganti Jenis Nasabah</label>
                    
                    <select name="jenis_nasabah" class="form-control" >
                        @foreach ($jns as $key => $row)
                            @if ($row == $arr->jenis_nasabah)
                                @php($sel = 'selected')
                            @else
                                @php($sel = '')
                            @endif
                                <option value="{{$row}}" {{$sel}}>{{ucwords($row)}} </option>

                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Status</label>
                    <input type="text" class="form-control" value="{{$arr->status_nasabah}}" readonly>
                </div>

                <div class="form-group">
                    <label>Ganti Status</label>
                    <select name="status_nasabah" class="form-control" >
                        @foreach ($sts as $key => $row)
                            @if ($row == $arr->status_nasabah)
                                @php($sel = 'selected')
                            @else
                                @php($sel = '')
                            @endif
                                <option value="{{$row}}" {{$sel}}>{{$row}} </option>

                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Kode Sales</label>
                    <input type="text" class="form-control" value="{{$arr->kodesales_nasabah}}" readonly>
                </div>

                <div class="form-group">
                    <label>Ganti Sales</label>
                    <select name="kodesales_nasabah" class="form-control" >
                        @foreach ($sls as $key => $row)
                            @if ($row == $arr->kodesales_nasabah)
                                @php($sel = 'selected')
                            @else
                                @php($sel = '')
                            @endif
                                <option value="{{$row}}" {{$sel}}>{{$row}} </option>

                        @endforeach
                    </select>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</section>

<script>
    setTimeout(function() {
        $('#successMessage').fadeOut('fast');
    }, 2000);

</script>
@stop
