@extends('main')
@section('content')

<section class="content-header">
    <h1>Nasabah <small>List Nasabah</small></h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-laptop"></i> List Nasabah</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    @if ($message = Session::get('success'))
        <div id="successMessage" class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>    
            <strong>{{ $message }}</strong>
        </div>
    @endif
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">List Nasabah</h3>
            <a href="{{route('print_nasabah')}}" title="print laporan nasabah" class="pull-right"><i class="fa fa-print fa-2x pull-right"></i> </a>

        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="table-data" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>ID Nasabah</th>
                        <th>Nama Nasabah</th>
                        <th>No HP</th>
                        <th>Keterangan</th>
                        <th>Jenis Nasabah</th>
                        <th>Kode Sales</th>
                        <th>Timestamp</th>
                        <th>Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @php($no = 0)
                    @forelse ($arr as $key => $row)
                        @php($no++)
                        <tr>
                            <td>{{$no}}</td>
                            <td>{{$row->id_nasabah}}</td>
                            <td>{{ucwords($row->nama_nasabah)}}</td>
                            <td>{{$row->hp_nasabah}}</td>
                            <td>{{$row->status_nasabah}}</td>
                            <td>{{ucwords($row->jenis_nasabah)}}</td>
                            <td>{{$row->kodesales_nasabah}}</td>
                            <td>{{$row->timestamp_nasabah}}</td>
                            <td>
                                <a href="{{route('editcommission',$row->id)}}" type="button" class="btn btn-primary">Edit</a>
                                <button type="button" data-item="{{$row->id}}" class="btn btn-danger delete">Delete</button>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td>Data nasabah kosong !</td>
                        </tr>
                    @endforelse                        


                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
</section>

<script>
    $(function () {
      $('#table-data').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : true
      })
    })
</script>

<script>
    $(document).on('click', '.delete', function () {
        Swal.fire({
            title: '',
            text: "Are  you sure want to delete this item ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    var id = $(this).attr('data-item');
                    var tbl= 'trx_nasabah   ';

                    $.ajax({
                        type:'POST',
                        url:"{{ route('delete_table') }}",
                        data:{
                            _token: "{{ csrf_token() }}",
                            id:id,
                            tbl:tbl
                        },
                        success:function(data){
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Delete Success !',
                                showConfirmButton: false,
                                timer: 1500
                            }).then(function () {
                                location.reload();
                            });
                        }
                    });
                }
            })
    });

</script>


@stop