<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('users')->insert([
            'name' => 'super_admin',
            'email' => 'mail@mail.com',
            '_id' => '12345',
            'password' => Hash::make('123'),
            'role_id' => '1'
        ]);

        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'mail@mail.com',
            '_id' => '11224',
            'password' => Hash::make('123'),
            'role_id' => '2'
        ]);

        DB::table('users')->insert([
            'name' => 'mitra',
            'email' => 'mail@mail.com',
            '_id' => '11223',
            'password' => Hash::make('123'),
            'role_id' => '3'
        ]);

        
    }
}
