<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('mst_role')->insert([
            'nama_role' => 'super_admin',
        ]);

        DB::table('mst_role')->insert([
            'nama_role' => 'admin',
        ]);

        DB::table('mst_role')->insert([
            'nama_role' => 'mitra',
        ]);
    }
}
