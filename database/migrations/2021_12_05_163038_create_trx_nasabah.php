<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxNasabah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_nasabah', function (Blueprint $table) {
            $table->id();
            $table->string('id_nasabah')->nullable();
            $table->string('nama_nasabah')->nullable();
            $table->string('hp_nasabah')->nullable();
            $table->string('status_nasabah')->nullable();
            $table->string('kodesales_nasabah')->nullable();
            $table->string('jenis_nasabah')->nullable();
            $table->string('timestamp_nasabah')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_nasabah');
    }
}
