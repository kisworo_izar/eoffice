<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxPartner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_partner', function (Blueprint $table) {
            $table->id();
            $table->string('partner_nama')->nullable();
            $table->string('partner_group')->nullable();
            $table->string('partner_hp')->nullable();
            $table->string('partner_email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_partner');
    }
}
