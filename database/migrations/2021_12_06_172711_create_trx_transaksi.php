<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_transaksi', function (Blueprint $table) {
            $table->id();
            $table->string('id_transaksi')->nullable();
            $table->string('id_nasabah')->nullable();
            $table->date('tanggal_transaksi')->nullable();
            $table->integer('transaksi_buy')->nullable();
            $table->integer('transaksi_sell')->nullable();
            $table->integer('transaksi_komisi')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_transaksi');
    }
}
