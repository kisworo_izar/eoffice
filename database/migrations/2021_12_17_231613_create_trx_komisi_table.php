<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxKomisiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_komisi', function (Blueprint $table) {
            $table->id();
            $table->date('bulan_komisi')->nullable();
            $table->string('jumlah_komisi')->nullable();
            $table->string('bukti_komisi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_komisi');
    }
}
