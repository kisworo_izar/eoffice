<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('login', ['as'=>'login_post','uses'=>'AuthController@login'] );
Route::get('login', ['as'=>'login','uses'=>'AuthController@ShowFormLogin'] );
Route::get('logout', ['as'=>'logout','uses'=>'AuthController@logout']);


// Route::middleware(['auth', 'role:admin'])->group(function () {
    Route::get('Home',['as'=>'Home','uses'=> 'AdminController@Home'])->middleware('role:1,2,3');
    Route::post('addadmin',['as'=>'addadmin','uses'=>'AdminController@AddAdmin'])->middleware('role:1');
    Route::get('ListAdministrator',['as'=>'ListAdministrator','uses'=> 'AdminController@ListAdministrator'])->middleware('role:1');
    Route::get('editadmin/{id}',['as'=>'editadmin','uses'=> 'AdminController@EditAdmin'])->middleware('role:1');
    Route::post('saveeditadmin',['as'=>'saveeditadmin','uses'=> 'AdminController@SaveEditAdmin'])->middleware('role:1');

    Route::get('AddNasabah',['as'=>'AddNasabah','uses'=>'AdminController@AddNasabah'])->middleware('role:1,2,3');
    Route::post('saveaddnasabah',['as'=>'saveaddnasabah','uses'=>'AdminController@SaveAddNasabah'])->middleware('role:1,2,3');
    Route::get('AddAdministrator',['as'=>'AddAdministrator','uses'=> 'AdminController@AddAdministrator'])->middleware('role:1,2,3');
    Route::get('ListNasabah',['as'=>'ListNasabah','uses'=> 'AdminController@ListNasabah'])->middleware('role:1,2,3');
    Route::post('delete_table',['as'=>'delete_table','uses'=>'AdminController@DeleteTable'])->middleware('role:1,2,3');
    Route::get('editnasabah/{id}',['as'=>'editnasabah','uses'=> 'AdminController@EditNasabah'])->middleware('role:1,2,3');
    Route::post('saveeditnasabah',['as'=>'saveeditnasabah','uses'=> 'AdminController@SaveEditNasabah'])->middleware('role:1,2,3');
    Route::get('print_nasabah',['as'=>'print_nasabah','uses'=> 'AdminController@PrintNasabah'])->middleware('role:1,2,3');


    Route::get('AddPartner',['as'=>'AddPartner','uses'=> 'AdminController@AddPartner'])->middleware('role:1,2');
    Route::get('ListPartner',['as'=>'ListPartner','uses'=> 'AdminController@ListPartner'])->middleware('role:1,2');
    Route::get('TrfPartner',['as'=>'TrfPartner','uses'=> 'AdminController@TrfPartner'])->middleware('role:1,2');

    Route::post('addpartner',['as'=>'addpartner','uses'=>'AdminController@SaveAddPartner'])->middleware('role:1,2');
    Route::get('editpartner/{id}',['as'=>'editpartner','uses'=> 'AdminController@EditPartner'])->middleware('role:1,2');
    Route::post('saveeditpartner',['as'=>'saveeditpartner','uses'=> 'AdminController@SaveeditPartner'])->middleware('role:1,2');
    Route::get('AddTransaksi',['as'=>'AddTransaksi','uses'=> 'AdminController@AddTransaksi'])->middleware('role:1,2');
    Route::post('saveaddtransaksi',['as'=>'saveaddtransaksi','uses'=>'AdminController@SaveAddTransaksi'])->middleware('role:1,2');
    Route::get('ListTransaksi',['as'=>'ListTransaksi','uses'=> 'AdminController@ListTransaksi'])->middleware('role:1,2');
    Route::get('edittransaksi/{id}',['as'=>'edittransaksi','uses'=> 'AdminController@EditTransaksi'])->middleware('role:1,2');
    Route::post('saveedittransaksi',['as'=>'saveedittransaksi','uses'=> 'AdminController@SaveEditTransaksi'])->middleware('role:1,2');

    Route::get('ReportTransaksi',['as'=>'ReportTransaksi','uses'=> 'AdminController@ReportTransaksi'])->middleware('role:1,2');
    Route::get('CustomReport',['as'=>'CustomReport','uses'=> 'AdminController@CustomReport'])->middleware('role:1,2');
    Route::get('CommisionReport',['as'=>'CommisionReport','uses'=> 'AdminController@CommisionReport'])->middleware('role:1,2');
    Route::post('addcommission',['as'=>'addcommission','uses'=>'AdminController@AddCommission'])->middleware('role:1,2');
    Route::get('ListCommission',['as'=>'ListCommission','uses'=> 'AdminController@ListCommission'])->middleware('role:1,2');
    Route::get('editcommission/{id}',['as'=>'editcommission','uses'=> 'AdminController@EditCommission'])->middleware('role:1,2');
    Route::post('saveeditcommission',['as'=>'saveeditcommission','uses'=> 'AdminController@SaveEditCommission'])->middleware('role:1,2');

    Route::get('TransactionReport',['as'=>'TransactionReport','uses'=> 'AdminController@TransactionReport'])->middleware('role:1,2,3');
    Route::get('UploadExcel',['as'=>'UploadExcel','uses'=> 'AdminController@UploadExcel'])->middleware('role:1,2,3');
    Route::post('file-import',['as'=>'file-import','uses'=> 'AdminController@fileImport'])->middleware('role:1,2');

    


    

    

    


    


    

    

// });



