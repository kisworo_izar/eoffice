<?php

if (!function_exists('ArForm')) {
    function ArForm($arr, $idkey){
        if (! $arr) return array();
        if (! is_array($arr)) return array();
        if (count($arr) == 0) return array();
        $str = array();
        foreach($arr as $row) $str[ $row[$idkey] ] = $row;
        return $str;
    }
}

function ToArr($value,$idkey){
    $data = json_decode(json_encode($value), true);
    $data = ArForm($data,$idkey);

    return $data;

}

function keArr($value){
    $data = json_decode(json_encode($value), true);

    return $data;

}

function stdToArray($obj){
    $reaged = (array)$obj;
    foreach($reaged as $key => &$field){
      if(is_object($field))$field = stdToArray($field);
    }
    return $reaged;
}

function ConvArr(Type $var = null){
    $data = DB::connection()->getPdo()
    ->query("SELECT * FROM users ")
    ->fetchAll(\PDO::FETCH_ASSOC);
}

function array_index($a, $subkey) {
    if (count($a) == 0) return array();
    foreach($a as $k=>$v) $b[$k] = strtolower($v[$subkey]);
    asort($b);
    foreach($b as $key=>$val) $c[] = $a[$key];
    return $c;
}

function array_flatten($array) { 
    if (!is_array($array)) { 
        return FALSE; 
    } 
    $result = array(); 
    foreach ($array as $key => $value) { 
        if (is_array($value)) { 
            $result = array_merge($result, array_flatten($value)); 
        } else { 
            $result[$key] = $value; 
        } 
    } 
    
    return $result; 
}

function status_nasabah(){
    $arr = array('CTH','Kefas','General','Prasmul Alumni','CTH-Client','Kata Dhana');
    return $arr;
}

function jenis_nasabah(){
    $arr = array('baru','pindah');
    return $arr;
}

function sales_nasabah(){
    $arr = array('F60','G20','G21');
    return $arr;
}

function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
	return $hasil_rupiah;
 
}

function nama_role(){
    $id_role = Auth::user()->role_id;
    $role    = collect(\DB::select('select nama_role from mst_role where id = ?', [$id_role]))->first();

    return $role;
}