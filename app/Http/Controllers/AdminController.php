<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use DB;
use Auth;
use Hash;
use Carbon\Carbon;
use PDF;
//tess
class AdminController extends Controller{

    public function Home(){
        $date      = \Carbon\Carbon::now();
        $skg       = $date->format('m');
        $bulan     =  $date->subMonth()->format('m');
        $bulanskg  =  \Carbon\Carbon::createFromFormat('m', $skg,'Asia/Jakarta')->format('F');
        $bulanlalu =  \Carbon\Carbon::createFromFormat('m', $bulan,'Asia/Jakarta')->format('F');



        $arr = collect(\DB::select("SELECT COUNT(*) nasabah FROM trx_nasabah "))->first();
        $nasabah = $arr->nasabah;

        $kms = collect(\DB::select("SELECT SUM(transaksi_komisi) komisi FROM trx_transaksi WHERE MONTH(tanggal_transaksi)='$skg' "))->first();
        $komisi = $kms->komisi;

        $tot = collect(\DB::select("SELECT SUM(transaksi_buy + transaksi_sell ) total_transaksi FROM trx_transaksi WHERE MONTH(tanggal_transaksi)='$skg' "))->first();
        $total_transaksi = $tot->total_transaksi;

        $kms_bulan_sebelumnya = collect(\DB::select("SELECT SUM(transaksi_komisi) komisi FROM trx_transaksi WHERE MONTH(tanggal_transaksi)='$bulan' "))->first();
        $komisi_mon_before = $kms_bulan_sebelumnya->komisi;

        $nsb  = DB::select("SELECT * FROM trx_nasabah ");
        $tes  = Auth::user()->name;
        // echo $tes;exit;
        // echo '<pre>';print_r($tes);exit;


        // echo $arr->komisi;exit;
        $data = array(
            'nasabah' => $nasabah,
            'komisi_skg'  => $komisi,
            'komisi_lalu' => $komisi_mon_before,
            'total_transaksi' => $total_transaksi,
            'bulanskg' => $bulanskg,
            'bulanlalu' => $bulanlalu,
            'nsb' => $nsb,
            'menu' => 'Home'
        );
        return view('Home.list')->with($data);

    }

    public function AddAdministrator(){
        $data = array(
            'menu' => 'AddAdministrator'
        );
        
        return view('AddAdministrator.list')->with($data);
    
    }
    
    public function AddAdmin(Request $request){
        $all        = $request->all();
        $name       = $request['admin_name'];
        $_id        = $request['admin_id'];
        $email      = $request['admin_email'];
        $password   = Hash::make($request['admin_pass']);
        $role_id    = 1;

        DB::insert("INSERT INTO users (name,email,_id,password,role_id) values (?, ?, ?, ?, ?)", [$name, $email,$_id,$password,$role_id]);

        return Redirect::to("/AddAdministrator")->withSuccess('Success Add Administrator');
    }

    public function ListAdministrator(Request $request){
        $arr  = DB::select("SELECT * FROM users WHERE role_id=1 ");
        $data = array(
            'menu' => 'ListAdministrator',
            'arr'  => $arr
        );
        return view('ListAdministrator.list')->with($data);

    }

    public function EditAdmin($id, Request $request){
        $arr = DB::table('users')->where('id',$id)->first();

        $data = array(
            'menu' => 'EditAdministrator',
            'arr'  => $arr
        );
        return view('EditAdministrator.list')->with($data);
    }

    public function SaveEditAdmin(Request $request){
        $all    = $request->all();
        $ueid   = $request['user_id'];
        $_id    = $request['admin_id'];
        $name   = $request['admin_name'];
        $email  = $request['admin_email'];
        $pass   = Hash::make($request['admin_pass']);

        DB::update("UPDATE users set _id ='$_id', name='$name', email='$email', password='$pass' WHERE id = ?", [$ueid]);

        return Redirect::to("/editadmin/$ueid")->withSuccess('Success Add Administrator');

    }

    public function DeleteTable(Request $request){
        $id  = $request['id'];
        $tbl = $request['tbl'];

        DB::delete("DELETE FROM $tbl WHERE id = ?", [$id]);
        if($tbl == 'users'){
            return Redirect::to("/ListAdministrator")->withSuccess('Success Add Administrator');
        }

        if($tbl == 'trx_partner'){
            return Redirect::to("/ListPartner")->withSuccess('Success Delete Partner');
        }

        if($tbl == 'trx_transaksi'){
            return Redirect::to("/ListTransaksi")->withSuccess('Success Delete Transaksi');
        }

    }

    public function AddNasabah(Request $request){
        $sls = sales_nasabah();
        $jns = jenis_nasabah();

        $data = array(
            'menu' => 'AddNasabah',
            'sls'  => $sls,
            'jns'  => $jns,
        );
        return view('AddNasabah.list')->with($data);
    }

    public function SaveAddNasabah(Request $request){
        $all        = $request->all();
        
        $id_nasabah         = $request['id_nasabah'];
        $nama_nasabah       = $request['nama_nasabah'];
        $hp_nasabah         = $request['hp_nasabah'];
        $status_nasabah     = $request['status_nasabah'];
        $jenis_nasabah      = $request['jenis_nasabah'];
        $kodesales_nasabah  = $request['kodesales_nasabah'];
        $timestamp_nasabah  = $request['timestamp_nasabah'];

        if($jenis_nasabah == 'baru'){
            $kd_sales = '';
        }else {
            $kd_sales = $kodesales_nasabah;
        }

        DB::insert("INSERT INTO trx_nasabah (id_nasabah,nama_nasabah,hp_nasabah,status_nasabah,jenis_nasabah,kodesales_nasabah,timestamp_nasabah) values (?, ?, ?, ?, ?, ?, ?)", [$id_nasabah, $nama_nasabah,$hp_nasabah,$status_nasabah, $jenis_nasabah, $kd_sales, $timestamp_nasabah]);

        return Redirect::to("/ListNasabah")->withSuccess('Success Add Nasabah !');
    }

    public function ListNasabah(){
        $arr  = DB::select("SELECT * FROM trx_nasabah ");

        $data = array(
            'menu' => 'ListNasabah',
            'arr'  => $arr
        );
        return view('ListNasabah.list')->with($data);
    }

    public function EditNasabah($id, Request $request){
        $arr = DB::table('trx_nasabah')->where('id',$id)->first();
        $sts = status_nasabah();
        $sls = sales_nasabah();
        $jns = jenis_nasabah();

        $data = array(
            'menu' => 'Edit Nasabah',
            'arr'  => $arr,
            'sts'  => $sts,
            'sls'  => $sls,
            'jns'  => $jns

        );
        return view('ListNasabah.edit')->with($data);
    }

    public function SaveEditNasabah(Request $request){
        $all = $request->all();
        $ueid           = $request['ueid'];
        $id_nasabah     = $request['id_nasabah'];
        $nama_nasabah   = $request['nama_nasabah'];
        $hp_nasabah     = $request['hp_nasabah'];
        $status_nasabah = $request['status_nasabah'];
        $kodesales_nasabah = $request['kodesales_nasabah'];
        $jenis_nasabah  = $request['jenis_nasabah'];

        if($jenis_nasabah == 'baru'){
            $kd_sales = '';
        }else {
            $kd_sales = $kodesales_nasabah;
        }

        DB::update("UPDATE trx_nasabah set id_nasabah ='$id_nasabah', nama_nasabah='$nama_nasabah', hp_nasabah='$hp_nasabah', status_nasabah='$status_nasabah', kodesales_nasabah='$kd_sales', jenis_nasabah='$jenis_nasabah'  WHERE id = ?", [$ueid]);

        // return Redirect::to("/editnasabah/$ueid")->withSuccess('Success Edit Nasabah');  
        return Redirect::to("/ListNasabah")->withSuccess('Success Edit Nasabah !');

    }

    public function PrintNasabah(Request $request){
        // echo 'tes';exit;
    	$nasabah = DB::select("SELECT * FROM trx_nasabah");
 
    	$pdf     = PDF::loadview('nasabah_pdf',['nasabah'=>$nasabah]);

    	return $pdf->download('laporan-nasabah-pdf');

    }

    public function AddPartner(Request $request){
        $data = array(
            'menu' => 'Add Partners'
        );
        
        return view('Partners.add')->with($data);
    }

    public function SaveAddPartner(Request $request){
        $all = $request->all();

        $partner_nama       = $request['partner_nama'];
        $partner_group      = $request['partner_group'];
        $partner_email      = $request['partner_email'];
        $partner_hp         = $request['partner_hp'];

        DB::insert("INSERT INTO trx_partner (partner_nama,partner_group,partner_email,partner_hp) values (?, ?, ?, ?)", [$partner_nama, $partner_group,$partner_email,$partner_hp]);

        return Redirect::to("/ListPartner")->withSuccess('Success Add Partner !');

    }

    public function ListPartner(Request $request){
        $arr  = DB::select("SELECT * FROM trx_partner ");

        $data = array(
            'menu' => 'List Partner',
            'arr'  => $arr
        );
        return view('Partners.list')->with($data);
    }

    public function EditPartner($id, Request $request){
        $arr = DB::table('trx_partner')->where('id',$id)->first();

        $data = array(
            'menu' => 'Edit Partner',
            'arr'  => $arr
        );
        return view('Partners.edit')->with($data);
    }

    public function SaveEditPartner(Request $request){
        // $all = $request->all();
        $ueid               = $request['ueid'];
        $partner_nama       = $request['partner_nama'];
        $partner_group      = $request['partner_group'];
        $partner_email      = $request['partner_email'];
        $partner_hp         = $request['partner_hp'];

        DB::update("UPDATE trx_partner set partner_nama ='$partner_nama', partner_group='$partner_group', partner_email='$partner_email', partner_hp='$partner_hp' WHERE id = ?", [$ueid]);

        return Redirect::to("/ListPartner")->withSuccess('Success Edit Partner');  
    }

    public function TrfPartner(Request $request){
        # code...
    }

    public function AddTransaksi(){
        $data = array(
            'menu' => 'AddTransaksi'
        );
        return view('Transaksi.add')->with($data);

    }

    public function SaveAddTransaksi(Request $request){
        $all = $request->all();

        $id_nasabah         = $request['id_nasabah'];
        $tanggal_transaksi  = $request['transaksi_date'];
        $transaksi_buy      = $request['transaksi_buy'];
        $transaksi_sell     = $request['transaksi_sell'];
        $transaksi_komisi   = $request['transaksi_komisi'];

        DB::insert("INSERT INTO trx_transaksi (id_nasabah,tanggal_transaksi,transaksi_buy,transaksi_sell, transaksi_komisi) values (?, ?, ?, ?, ?)", [$id_nasabah, $tanggal_transaksi,$transaksi_buy,$transaksi_sell, $transaksi_komisi]);

        return Redirect::to("/ListTransaksi")->withSuccess('Success Add Transaksi !');

    }

    public function ListTransaksi(){
        $arr  = DB::select("SELECT a.id,a.id_nasabah,a.tanggal_transaksi, a.transaksi_buy,a.transaksi_sell,a.transaksi_komisi,b.nama_nasabah,b.status_nasabah FROM trx_transaksi a LEFT JOIN trx_nasabah b ON a.id_nasabah = b.id_nasabah");

        $data = array(
            'menu' => 'ListTransaksi',
            'arr'  => $arr
        );
        return view('Transaksi.list')->with($data);

    }

    public function EditTransaksi($id, Request $request){
        $arr = DB::table('trx_transaksi')->where('id',$id)->first();

        $data = array(
            'menu' => 'Edit Transaksi',
            'arr'  => $arr
        );
        return view('Transaksi.edit')->with($data);
    }

    public function SaveEditTransaksi(Request $request){
        $all                 = $request->all();
        $ueid                = $request['ueid'];
        $tanggal_transaksi   = $request['transaksi_date'];
        $transaksi_buy       = $request['transaksi_buy'];
        $transaksi_sell      = $request['transaksi_sell'];
        $transaksi_komisi    = $request['transaksi_komisi'];


        DB::update("UPDATE trx_transaksi set tanggal_transaksi ='$tanggal_transaksi', transaksi_buy='$transaksi_buy', transaksi_sell='$transaksi_sell', transaksi_komisi='$transaksi_komisi' WHERE id = ?", [$ueid]);

        return Redirect::to("/ListTransaksi")->withSuccess('Success Add Administrator');

    }

    public function ReportTransaksi(){
        $month = Carbon::now()->month;
        $val = DB::connection()->getPdo()
                ->query("SELECT SUM(transaksi_buy) transaksi_buy, SUM(transaksi_sell) transaksi_sell,SUM(transaksi_komisi) transaksi_komisi, SUM(transaksi_buy + transaksi_sell) total, id_nasabah FROM trx_transaksi WHERE month(tanggal_transaksi)='$month' group by id_nasabah ORDER BY total")
                ->fetchAll(\PDO::FETCH_ASSOC);

        $val   = ToArr($val,'id_nasabah');
        $nsb   = DB::connection()->getPdo()
        ->query("SELECT *,'' total, '' transaksi_buy, '' transaksi_sell, '' transaksi_komisi FROM trx_nasabah")
        ->fetchAll(\PDO::FETCH_ASSOC);
        
        $nsb   = ToArr($nsb,'id_nasabah');

        foreach($nsb as $key => $row){
            if(array_key_exists($row['id_nasabah'],$val)){
                $nsb[$key]['total'] = $val[$row['id_nasabah']]['total'];
                $nsb[$key]['transaksi_buy']     = $val[$row['id_nasabah']]['transaksi_buy'];
                $nsb[$key]['transaksi_sell']    = $val[$row['id_nasabah']]['transaksi_sell'];
                $nsb[$key]['transaksi_komisi']  = $val[$row['id_nasabah']]['transaksi_komisi'];
            }
        }


        $total = DB::select("SELECT SUM(transaksi_buy + transaksi_sell) value , b.status_nasabah label FROM trx_transaksi a LEFT JOIN trx_nasabah b ON a.id_nasabah=b.id_nasabah WHERE month(tanggal_transaksi)='$month' GROUP BY b.status_nasabah ");

        $data = array(
            'arr'  => $nsb,
            'total'=> $total,
            'menu' => 'Report Transaksi'
        );
        return view('Report.reporttransaksi')->with($data);
    }

    public function CustomReport(Request $request){
        $sel_tgl = $request['sel_tgl'];
        $sel     = explode('-',$sel_tgl);
        $month   = $sel[0];
        if($sel_tgl == ""){
            $date           = \Carbon\Carbon::now();
            $bulan          =  $date->subMonth()->format('m');
        }else {
            $bulan = $month;
        }
        $indobulan =  \Carbon\Carbon::createFromFormat('m', $bulan,'Asia/Jakarta')->format('F');

        
        $val = DB::connection()->getPdo()
                ->query("SELECT SUM(transaksi_buy) transaksi_buy, SUM(transaksi_sell) transaksi_sell,SUM(transaksi_komisi) transaksi_komisi, SUM(transaksi_buy + transaksi_sell) total, id_nasabah FROM trx_transaksi WHERE month(tanggal_transaksi)='$month' group by id_nasabah ORDER BY total")
                ->fetchAll(\PDO::FETCH_ASSOC);

        $val   = ToArr($val,'id_nasabah');
        $nsb   = DB::connection()->getPdo()
        ->query("SELECT *,'' total, '' transaksi_buy, '' transaksi_sell, '' transaksi_komisi FROM trx_nasabah")
        ->fetchAll(\PDO::FETCH_ASSOC);
        
        $nsb   = ToArr($nsb,'id_nasabah');

        foreach($nsb as $key => $row){
            if(array_key_exists($row['id_nasabah'],$val)){
                $nsb[$key]['total'] = $val[$row['id_nasabah']]['total'];
                $nsb[$key]['transaksi_buy']     = $val[$row['id_nasabah']]['transaksi_buy'];
                $nsb[$key]['transaksi_sell']    = $val[$row['id_nasabah']]['transaksi_sell'];
                $nsb[$key]['transaksi_komisi']  = $val[$row['id_nasabah']]['transaksi_komisi'];
            }
        }


        $total = DB::select("SELECT SUM(transaksi_buy + transaksi_sell) value , b.status_nasabah label FROM trx_transaksi a LEFT JOIN trx_nasabah b ON a.id_nasabah=b.id_nasabah WHERE month(tanggal_transaksi)='$month' GROUP BY b.status_nasabah ");

        
        $data   = array(
            'arr'  => $val,
            'total'=> $total,
            'bln'  => $indobulan,
            'sel_tgl' => $sel_tgl,
            'menu' => 'Custom Report Transaksi'
        );

        return view('Report.costumreport')->with($data);
    }

    public function CommisionReport(Request $request){
        $data   = array(
            'menu' => 'Custom Report Transaksi'
        );

        return view('Report.commisionreport')->with($data);
    }

    public function AddCommission(Request $request){
        $arr            = $request->all();
        $bulan_komisi   = $request['bulan_komisi'];
        $jumlah_komisi  = $request['jumlah_komisi'];
        $file           = $request->file('file');
		$nama_file      = $file->getClientOriginalName();
		$tujuan_upload  = 'data_file';
		
        
        $file->move($tujuan_upload,$nama_file);

        DB::insert("INSERT INTO trx_komisi (bulan_komisi,jumlah_komisi,bukti_komisi) values (?, ?, ?)", [$bulan_komisi, $jumlah_komisi,$nama_file]);

        return Redirect::to("/ListCommission")->withSuccess('Success Add Commission !');

    }

    public function ListCommission(Request $request){
        $arr    = DB::select("SELECT * FROM trx_komisi ");
        $data   = array(
            'menu' => 'List Commission',
            'arr'  => $arr
        );

        return view('Report.listcommission')->with($data);
    }

    public function EditCommission($id, Request $request){
        $arr = DB::table('trx_komisi')->where('id',$id)->first();

        $data = array(
            'menu' => 'Edit Commission',
            'arr'  => $arr
        );
        return view('Report.editcommission')->with($data);
    }

    public function SaveEditCommission(Request $request){
        $all = $request->all();
        $id             = $request['_id'];
        $bulan_komisi   = $request['bulan_komisi'];
        $jumlah_komisi  = $request['jumlah_komisi'];
        $file           = $request->file('file');
        if($file){
            $nama_file      = $file->getClientOriginalName();
            $tujuan_upload  = 'data_file';
            $file->move($tujuan_upload,$nama_file);
            $updatefile = ", bukti_komisi='$nama_file'";

        }else {
            $nama_file = "";
            $updatefile = "";
        }
		
		
        DB::update("UPDATE trx_komisi set bulan_komisi ='$bulan_komisi', jumlah_komisi='$jumlah_komisi' $updatefile WHERE id = ?", [$id]);

        return Redirect::to("/ListCommission")->withSuccess('Success Edit Commission');  
    }

    public function TransactionReport(Request $request){

        $sel_tgl = $request['sel_tgl'];
        $sel     = explode('-',$sel_tgl);
        $month   = $sel[0];
        if($sel_tgl == ""){
            $date           = \Carbon\Carbon::now();
            $bulan          =  $date->subMonth()->format('m');
        }else {
            $bulan = $month;
        }
        $indobulan =  \Carbon\Carbon::createFromFormat('m', $bulan,'Asia/Jakarta')->format('F');
        
        $val = DB::connection()->getPdo()
        ->query("SELECT SUM(transaksi_buy) transaksi_buy, SUM(transaksi_sell) transaksi_sell,SUM(transaksi_komisi) transaksi_komisi, SUM(transaksi_buy + transaksi_sell) total, id_nasabah FROM trx_transaksi WHERE month(tanggal_transaksi)='$bulan' group by id_nasabah ORDER BY total")
        ->fetchAll(\PDO::FETCH_ASSOC);

        $val   = ToArr($val,'id_nasabah');
        $nsb   = DB::connection()->getPdo()
        ->query("SELECT *,'' total, '' transaksi_buy, '' transaksi_sell, '' transaksi_komisi FROM trx_nasabah")
        ->fetchAll(\PDO::FETCH_ASSOC);

        $nsb   = ToArr($nsb,'id_nasabah');

        foreach($nsb as $key => $row){
            if(array_key_exists($row['id_nasabah'],$val)){
                $nsb[$key]['total'] = $val[$row['id_nasabah']]['total'];
                $nsb[$key]['transaksi_buy']     = $val[$row['id_nasabah']]['transaksi_buy'];
                $nsb[$key]['transaksi_sell']    = $val[$row['id_nasabah']]['transaksi_sell'];
                $nsb[$key]['transaksi_komisi']  = $val[$row['id_nasabah']]['transaksi_komisi'];
            }
        }


        $total = DB::select("SELECT SUM(transaksi_buy + transaksi_sell) value , b.status_nasabah label FROM trx_transaksi a LEFT JOIN trx_nasabah b ON a.id_nasabah=b.id_nasabah WHERE month(tanggal_transaksi)='$bulan' GROUP BY b.status_nasabah ");

        
        $data   = array(
            'arr'  => $nsb,
            'total'=> $total,
            'bln'  => $indobulan,
            'sel_tgl' => $sel_tgl,
            // 'tot'  => $tot,
            'menu' => 'Transaction Report'
        );

        return view('Report.transactionreport')->with($data);
    }

    public function UploadExcel(Request $request){

        $data = array(
            'menu' => 'Upload Excel Transaksi',
        );
        return view('Upload.list')->with($data);
    }

    public function fileImport(Request $request){
        $file = $request->file('uploaded_file');
        if ($file) {
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension(); //Get extension of uploaded file
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize(); //Get size of uploaded file in bytes//Check for file extension and size
            $this->checkUploadedFileProperties($extension, $fileSize);
            //Where uploaded file will be stored on the server 
            $location = 'uploads'; //Created an "uploads" folder for that
            // Upload file
            $file->move($location, $filename);
            // In case the uploaded file path is to be stored in the database 
            $filepath = public_path($location . "/" . $filename);
            // Reading file
            $file = fopen($filepath, "r");
            $importData_arr = array(); // Read through the file and store the contents as an array
            $i = 0;
            //Read the contents of the uploaded file 
            while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                $num = count($filedata);
                // Skip first row (Remove below comment if you want to skip the first row)
                // if ($i == 0) {
                //     $i++;
                //     continue;
                // }
                for ($c = 0; $c < $num; $c++) {
                $importData_arr[$i][] = $filedata[$c];
                }
                $i++;
            }
            // echo '<pre>';print_r($importData_arr);exit;

            fclose($file); 

            foreach($importData_arr as $key => $row){
                $id_transaksi   = $row[0];
                $id_nasabah     = $row[1];
                $tanggal_trans  = $row[2];
                $buy            = $row[3];
                $sell           = $row[4];
                $komisi         = $row[5];

                DB::insert("INSERT INTO trx_transaksi (id_transaksi,id_nasabah,tanggal_transaksi,transaksi_buy,transaksi_sell,transaksi_komisi) values (?, ?, ?, ?, ?, ?)", [$id_transaksi, $id_nasabah,$tanggal_trans,$buy,$sell,$komisi]);

            }

        }
    }

    public function checkUploadedFileProperties($extension, $fileSize){
        $valid_extension = array("csv", "xlsx"); //Only want csv and excel files
        $maxFileSize = 2097152; // Uploaded file size limit is 2mbif (in_array(strtolower($extension), $valid_extension)) {
        if ($fileSize <= $maxFileSize) {
        } else {
            throw new \Exception('No file was uploaded', Response::HTTP_REQUEST_ENTITY_TOO_LARGE); //413 error}
        } 
    }


}
