<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EnsureUserHasRole
{
    public function handle(Request $request, Closure $next, ...$roles){
        if(Auth::check()){
            $user = Auth::user();
            // echo '<pre>'; print_r($roles);
            // echo $user->role_id;
            if(in_array($user->role_id,$roles)){
                // echo 'a';
                return $next($request);
            }
        }
        // echo 'b';exit;
        return redirect()->to('logout');

    }
}